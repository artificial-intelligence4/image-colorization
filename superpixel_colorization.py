import numpy as np
import cv2

def colorize_superpixels(superpixels, colors):
    colorized_superpixels = []

    for superpixel, color in zip(superpixels, colors):
        superpixel = superpixel[...,0]

        a_channel = np.zeros((superpixel.shape[0], superpixel.shape[1]))
        b_channel = np.zeros((superpixel.shape[0], superpixel.shape[1]))

        a_channel[superpixel > 0] = color[0]
        b_channel[superpixel > 0] = color[1]

        colorized = cv2.merge((superpixel, a_channel.astype('uint8'), b_channel.astype('uint8')))

        colorized_superpixels.append(colorized)
    
    return colorized_superpixels