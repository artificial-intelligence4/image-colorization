# Image colorization

#### Instructions
1. Create a virual environment
`python -m venv .venv`
<br>

2. Activate the virual environment
`source .venv/bin/activate`
<br>

3. Install the dependencies
`pip install -r requirements.txt`
<br>

4. Run
`python3 main.py`
<br>
