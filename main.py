from os import read
import cv2
import numpy as np
import matplotlib.pyplot as plt
from lab import LABDataset, LABDiscretizer
from superpixel_segmentation import SLICClustering
from feature_extraction import FeatureExtractor
from superpixel_colorization import colorize_superpixels
from sklearn.svm import SVC
import argparse

def parseArgs():
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--image", required = True, help = "Path to the image")
    ap.add_argument("-d", "--data", required = True, help = "Path to the training dataset")
    ap.add_argument("-s", "--superpixels", default=20, help = "The number of superpixels to segment the images", type=int)
    ap.add_argument("-c", "--colors", default=5, help = "The number of colors to discretize the LAB colorspace", type=int)
    return vars(ap.parse_args())

def readImage(image):
    im = cv2.imread(image)

    return cv2.cvtColor(im, cv2.COLOR_BGR2LAB)

def mergeSuperpixels(superpixels):
    width, height, _ = superpixels[0].shape
    img = np.zeros((width, height, 3), dtype = "uint8")

    for superpixel in superpixels:
        img = cv2.bitwise_or(img, superpixel)
    
    return img

if __name__ == '__main__':

    args = parseArgs()

    # Images to LAB colorspace
    lab_images = LABDataset(args['data']).images
    print(f'\n{len(lab_images)} sample images found')

    # Lab colorspace discretization
    discretizer = LABDiscretizer(n_colors=args['colors'])
    print("\nDiscretization started...")
    discretizer.fit(lab_images)
    print("Discretization finished...")

    colors = discretizer.colors
    print(f'\nLAB discretized in {len(colors)} colors')

    # SLIC Superpixels
    slic = SLICClustering()
    superpixels, label_colors = slic.segment(lab_images, args["superpixels"])
    assert len(superpixels) == len(label_colors)

    labels = discretizer.predict(label_colors)

    print(f'\nGathered {len(superpixels)} superpixels')

    # Feature Exctraction
    print('\nFeature extraction started...')
    feature_extractor = FeatureExtractor()
    features = feature_extractor.extract(superpixels)
    print('Feature extraction finished...')

    # Train color predictor
    print('\nTraining color predictor...')
    color_predictor = SVC()
    color_predictor.fit(features, labels)
    print('Color predictor training finished...')

    print('\n', 50 * '-')

    # Target image
    target = readImage(args["image"])
    L = cv2.split(target)[0]

    # Target SLIC Superpixels
    print('\nCollecting target image superpixels...')
    target_superpixels, _ = slic.segment([target], args["superpixels"])
    print(f'\nSegmented target image into {len(target_superpixels)} superpixels')

    # Target features
    print('\nExtracting feature vectors from each superpixel...')
    target_features = feature_extractor.extract(target_superpixels)

    target_labels = color_predictor.predict(target_features)
    target_superpixel_colors = np.array(
        [colors[label] for label in target_labels]
    )
    print('Feature vector extraction finished...')

    print('\nColorizing each superpixel...')
    colorized_superpixels = colorize_superpixels(target_superpixels, target_superpixel_colors)
    print('Target superpixel colorization finished...')

    print('\nMerging superpixels...')
    img = mergeSuperpixels(colorized_superpixels)

    img = cv2.merge((L, img[...,1], img[...,2]))

    print('\nImage colorization finished!')

    plt.figure("Colorized Image")
    plt.imshow(cv2.cvtColor(img, cv2.COLOR_LAB2RGB))
    plt.show()
