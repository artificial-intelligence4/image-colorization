import cv2
from skimage.filters import gabor
import numpy as np

class FeatureExtractor:
    '''Encapsulates methods that handle feature extraction from superpixels'''
    
    def __init__(self):
        self.orb = cv2.ORB_create(50)

    def extract(self, superpixels):
        '''
        Calculates the feature vectors for each superpixel

        A feature vector is in the form of
        
        [G1, G2, ..., Gn, O1, O2, ... On]

        where Gn is the n-th Gabor feature and On is the n-th ORB feature.
        '''

        features = []

        for superpixel in superpixels:
            grayscale_superpixel = cv2.split(superpixel)[0]
            gabor_features = self.__extract_gabor_features(grayscale_superpixel)
            orb_features = self.__extract_orb_features(grayscale_superpixel)
            
            features.append(np.append(gabor_features, orb_features))
            
        return features

    def __extract_gabor_features(self, superpixel):
        '''Extracts Gabor features for a given superpixel'''

        gabor_features = np.array([])

        # Rotate the filter 4 times by 45 degrees
        for k in range(4):
            theta = k * (np.pi / 4)
            filt_real, filt_imag = gabor(superpixel, frequency=0.6, theta=theta)

            filter_vector = [np.median(filt_real.reshape(filt_real.shape[0] * filt_real.shape[1]))]
            gabor_features = np.append(gabor_features, filter_vector)

        return gabor_features


    def __extract_orb_features(self, superpixel):
        '''Extracts ORB features for a given superpixel'''

        keypoints, descriptors = self.orb.detectAndCompute(superpixel, None)
        orb_features = np.median(descriptors, axis=0) if len(keypoints) > 0 else 32 * [0]

        return orb_features
