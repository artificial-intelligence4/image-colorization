import os
import cv2
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans


class LABDataset:
    '''Represents an image dataset in the LAB colorspace'''

    def __init__(self, image_dir):
        image_paths = [os.path.join(image_dir, image) for image in os.listdir(image_dir)]

        # Read images
        bgr_images = [cv2.imread(path) for path in image_paths]
        
        # Convert images to LAB colorspace
        self.images = [cv2.cvtColor(bgr, cv2.COLOR_BGR2LAB) for bgr in bgr_images]


class LABDiscretizer:
    '''Discretizes the LAB colorspace based on a set of training images'''

    def __init__(self, n_colors):
        self.n_colors = n_colors
        self.kmeans = KMeans(n_clusters=self.n_colors)

    def fit(self, images, visual=False):
        # Collect colors from all the images
        data = []

        for image in images:
            AB = image[..., 1:]

            height, width, channels = AB.shape

            colors = AB.reshape(height * width, channels)

            for color in colors:
                data.append(color)

        data = np.array(data)

        # Train        
        self.kmeans.fit(data)
        self.colors = self.kmeans.cluster_centers_

        if visual:
            plt.figure('LAB Colorspace Discretization')
            plt.xlabel('A')
            plt.ylabel('B')
            plt.scatter(data[..., 0], data[..., 1], marker='o', c='k')
            plt.scatter(self.colors[..., 0], self.colors[..., 1], marker='x', c='r')
            plt.show()

    def predict(self, colors, visual=False):
        '''Returns the nearest centroid for each given color'''

        colors = np.array(colors)
        
        if visual:
            plt.figure('Superpixel Color Discretization')
            plt.xlabel('A')
            plt.ylabel('B')
            plt.scatter(colors[..., 0], colors[..., 1], marker='+', c='g')
            plt.scatter(self.colors[..., 0], self.colors[..., 1], marker='x', c='r')
            plt.show()

        return self.kmeans.predict(colors)
