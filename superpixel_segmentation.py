import os
import cv2
import numpy as np
#libraries for clustering superpixels with SLIC algorithm
from skimage.segmentation import slic
from skimage.util import img_as_float

class SLICClustering:
    '''Handles the SLIC superpixel calculation process for the given images'''

    def segment(self, images, n_segments):
        '''Calculates `n_segemnts` superpixels for each image'''

        superpixels = []
        label_colors = []
        
        for image in images:
            segments = slic(img_as_float(image), n_segments, sigma=5, start_label=1)

            # Loop over the unique segment values
            for segment_value in np.unique(segments):
                # Segment mask
                mask = np.zeros(image.shape[:2], dtype = "uint8")
                mask[segments == segment_value] = 255
                superpixels.append(cv2.bitwise_and(image, image, mask = mask))

                # Superpixel median AB color
                colors = image[segments == segment_value]
                median_color = [np.median(colors[:,1]), np.median(colors[:,2])]
                label_colors.append(median_color)
        
        return (superpixels, label_colors)
          